;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Lele's own Doom setup
;; :Created:   dom 19 dic 2021, 19:14:36
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(doom!
 :completion
 (company +tng)
 (vertico +icons)

 :ui
 deft
 (emoji +unicode)
 indent-guides
 nav-flash

 :emacs
 (dired +icons)

 :term
 shell

 :checkers
 (spell +flyspell +aspell)

 :tools
 docker
 lsp
 pass
 pdf
 rgb

 :lang
 elixir
 javascript
 json
 latex
 nix
 (org +pretty)
 plantuml
 (python +lsp +pyright +cython)
 ;;rest
 rst
 web
 yaml

 :email
 notmuch

 :app
 irc

 :my
 gnus
 javascript)
