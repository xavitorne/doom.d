;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Required packages
;; :Created:   mar 14 dic 2021, 11:02:15
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

(package! google-translate
  :pin "0f7f48a09bca064999ecea03102a7c96f52cbd1b")

(package! iedit
  :pin "44968bea9bff8fdd5bf9d227f53814c44bb9f619")
