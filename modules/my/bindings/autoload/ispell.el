;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Spelling check helpers
;; :Created:   ven 10 dic 2021, 13:12:00
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;;;###autoload
(defun my+ispell/cycle-languages ()
  "Select the next spell dictionary from the ring."
  (interactive)
  (let ((lang (ring-ref my+ispell/languages -1)))
    (ring-insert my+ispell/languages lang)
    (ispell-change-dictionary lang)))
