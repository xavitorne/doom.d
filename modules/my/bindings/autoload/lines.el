;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Open new line helpers
;; :Created:   ven 10 dic 2021, 13:12:28
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;;;###autoload
(defun my/open-next-line (arg)
  "Move to the next line and then open a line."
  (interactive "p")
  (end-of-line)
  (open-line arg)
  (forward-line 1)
  (indent-according-to-mode))

;;;###autoload
(defun my/open-previous-line (arg)
  "Open a new line before the current one."
  (interactive "p")
  (beginning-of-line)
  (open-line arg)
  (indent-according-to-mode))

;;;###autoload
(defun my/scroll-up-one-line ()
  "Scroll window up by one line."
  (interactive)
  (scroll-up-command 1))

;;;###autoload
(defun my/scroll-down-one-line ()
  "Scroll window down by one line."
  (interactive)
  (scroll-down-command 1))
