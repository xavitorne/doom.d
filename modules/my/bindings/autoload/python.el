;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Python helpers
;; :Created:   ven 10 dic 2021, 13:12:43
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;;;###autoload
(defun my+python/split-string ()
  "Split string at point."
  (interactive)
  (let ((ssp (python-syntax-context 'string)))
    (when ssp
      (let ((ssqc (char-after ssp))
            (fstr (eq (char-before ssp) ?f))
            (ws (progn
                  (looking-at "\s*")
                  (match-string-no-properties 0))))
        (insert ssqc)
        (newline-and-indent)
        (when fstr
          (insert ?f))
        (insert ssqc)
        (insert ws)))))
