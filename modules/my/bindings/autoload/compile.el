;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Compilation helpers
;; :Created:   ven 10 dic 2021, 13:11:20
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(autoload 'compilation-read-command "compile")

;;;###autoload
(defun my+compile/next-makefile (command)
  "Run a compilation after changing the working directory to nearest `Makefile'."
  (interactive
   (list
    (let ((command (eval compile-command)))
      (if (or compilation-read-command current-prefix-arg)
          (compilation-read-command command)
        command))))
  (let* ((root-dir (or (locate-dominating-file default-directory "Makefile") "."))
         (cd-command (concat "cd " root-dir " && " command)))
    (compile cd-command)
    (setq compile-command command)))
