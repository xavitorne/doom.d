;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — My function keys
;; :Created:   ven 10 dic 2021, 13:10:23
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(require 'kmacro)

;; F1 is a keymap under Doom, silly to loose that just for hippie-expand, still
;; available as C-/

(keymap-global-set "<f2>"    #'query-replace)
(keymap-global-set "C-<f2>"  #'map-query-replace-regexp)
(keymap-global-set "S-<f2>"  #'query-replace-regexp)
(when (fboundp 'iedit-mode)
  (keymap-global-set "M-<f2>"  #'iedit-mode))

(keymap-global-set "<f3>"    #'grep)
(when (fboundp 'consult-ripgrep)
  (keymap-global-set "C-<f3>"  #'consult-ripgrep))
(keymap-global-set "S-<f3>"  #'grep-find)
(keymap-global-set "M-<f3>"  #'my+git/grep)
(when (fboundp 'consult-git-grep)
  (keymap-global-set "M-S-<f3>"  #'consult-git-grep))

(keymap-global-set "<f4>"    #'kmacro-end-or-call-macro)
(keymap-global-set "C-<f4>"  kmacro-keymap)
(keymap-global-set "S-<f4>"  #'kmacro-start-macro-or-insert-counter)
(keymap-global-set "M-<f4>"  #'kmacro-edit-macro-repeat)

(keymap-global-set "<f5>"    #'my+compile/next-makefile)

(keymap-global-set "<f6>"    #'next-error)
(keymap-global-set "S-<f6>"  #'previous-error)

(keymap-global-set "<f7>"    #'flycheck-next-error)
(keymap-global-set "S-<f7>"  #'flycheck-previous-error)

(keymap-global-set "<f8>"    #'my+ispell/cycle-languages)

(keymap-global-set "<f9>"    #'magit-status)
(keymap-global-set "S-<f9>"  #'magit-blame-addition)
(keymap-global-set "M-<f9>"  #'magit-log-buffer-file)

(keymap-global-set "<f10>"   #'shell)
(keymap-global-set "S-<f10>" #'eshell)
(keymap-global-set "M-<f10>" #'term)

(keymap-global-set "<f11>"   #'pyvenv-activate)

(keymap-global-set "<f12>"   doom-leader-toggle-map)
