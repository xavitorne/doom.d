;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults configuration
;; :Created:   ven 10 dic 2021, 13:13:37
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

(when (featurep! +ui)
  (load! "+ui"))

(when (featurep! +modes)
  (load! "+modes"))

(when (featurep! +packages)
  (load! "+packages"))

(load! (concat "config-" user-login-name) nil t)
