;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults modes configuration
;; :Created:   ven 10 dic 2021, 13:14:35
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021, 2022 Lele Gaifax
;;

;; By default there's no Lisp-powered scratch buffer in Doom, and that's bad for
;; me, so restore the lisp interaction mode
(setq! doom-scratch-initial-major-mode 'lisp-interaction-mode)

;; Disable automatic completion, something I'm not used to and that I find
;; highly distracting: I know when I need some help, so I prefer manually
;; triggering the facility, thanks.
(after! company
  (setq! company-idle-delay nil)
  (keymap-global-set "C-<tab>" #'company-indent-or-complete-common))

;; Disable invasive lsp-mode features, see lsp-mode docs/tutorials/how-to-turn-off.md
(after! lsp-mode
  (setq! lsp-enable-symbol-highlighting nil))
(after! lsp-ui
  (setq! lsp-ui-sideline-enable nil     ; not anymore useful than flycheck
         lsp-ui-doc-enable nil          ; slow and redundant with K
         ))
;; If there isn't a server, rest assured it's on purpose
(setq! +lsp-prompt-to-install-server 'quiet)

;; Configure OrgMode clocksum format
(after! org-mode
  (setq! org-time-clocksum-format
         '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))

;; Adjust whitespace checkers
(use-package! whitespace
  :config
  (setq! whitespace-style '(face
                            trailing
                            lines-tail
                            empty
                            space-before-tab
                            space-after-tab
                            indentation
                            indentation::space
                            tabs)
         whitespace-global-modes '(prog-mode))

  (defun my/refresh-after-fill-column-change (symbol newval op where)
    (when (and where
               (or (default-value 'global-whitespace-mode) whitespace-mode))
      (with-current-buffer where
        (font-lock-flush))))

  (add-variable-watcher 'fill-column #'my/refresh-after-fill-column-change)

  (global-whitespace-mode +1))

;; This is not a typewriter, do not insert TABs
(setq! tab-always-indent t)
