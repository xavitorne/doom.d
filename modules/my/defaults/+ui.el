;;; -*- coding: utf-8; lexical-binding: t; -*-
;; :Project:   doom.d — Defaults UI configuration
;; :Created:   ven 10 dic 2021, 13:15:56
;; :Author:    Lele Gaifax <lele@metapensiero.it>
;; :License:   GNU General Public License version 3 or later
;; :Copyright: © 2021 Lele Gaifax
;;

;; Having line numbers everywhere is a waste of space; use `C-c t l' to toggle
;; them when needed
(setq! display-line-numbers-type nil)

;; Customize the dashboard, showing a nicer image...
(setq! fancy-splash-image (expand-file-name "splash.png" (dir!)))

;; ... and possibly a random fortune in the footer
(when (executable-find "fortune")
  (defun doom-dashboard-widget-footer ()
    (insert "\n")
    (mapc (lambda (line)
            (insert
             (propertize (+doom-dashboard--center +doom-dashboard--width line)
                         'face 'doom-dashboard-footer)
             "\n"))
          (split-string
           (with-output-to-string
             (call-process "fortune" nil standard-output nil "-s"))
           "\n" t))))
